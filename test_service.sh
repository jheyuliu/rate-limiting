#!/bin/bash

for i in {1..15}
do
  USER_ID="user$i"
  
  curl -X GET -H "X-User-Id: $USER_ID" http://localhost:5000/
  
  sleep 1
done