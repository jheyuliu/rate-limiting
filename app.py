from flask import Flask, jsonify, make_response, request, g
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import yaml

app = Flask(__name__)
app.config['REDIS_URL'] = "redis://redis:6379/0"
limiter = Limiter(app=app, key_func=get_remote_address, storage_uri=app.config['REDIS_URL'])

user_limits = {}
default_limit = ""
custom_route_limit = ""

def load_config():
    global user_limits, default_limit, custom_route_limit
    with open("config.yaml", "r") as config_file:
        config = yaml.safe_load(config_file)
        user_limits = config.get('user_limits', {})
        default_limit = config.get('rate_limits', {}).get('default_limit', '')
        custom_route_limit = config.get('rate_limits', {}).get('custom_route_limit', '')

load_config()

@limiter.request_filter
def get_user_key():
    user_id = request.headers.get('X-User-Id', 'anonymous')
    if user_id not in user_limits:
        g.user_id = 'anonymous'
    else:
        g.user_id = user_id
    return

@app.route('/', methods=['GET'])
@limiter.limit(lambda: custom_route_limit if g.user_id == 'anonymous' else user_limits[g.user_id])
def main():
    print(user_limits[g.user_id])
    return jsonify({"data": "This request has passed."})

@app.errorhandler(429)
def ratelimit_handler(e):
    return make_response(
        jsonify(error="ratelimit exceeded %s" % e.description)
        , 429
    )

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)