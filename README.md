# Rate-Limited Flask Service

This repository contains a Flask-based web service with rate-limiting capabilities. The service is Dockerized and orchestrated using Docker Compose. It utilizes the Flask framework and Flask Limiter extension for controlling the rate of incoming requests.

## Prerequisites
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Quick Start

### 1. Clone the Repository
```bash
git clone <repository-url>
cd <repository-directory>
```

### 2. Start the Service
```bash
./start.sh
```

### 3. Run Tests
```bash
./test_service.sh
```

## Service Details

- The service reads configuration from `config.yaml` for user-specific rate limits and default limits.
- Requests are filtered through the `X-User-Id` header, and users are categorized as anonymous if not found in the configured limits.
- The main route ("/") is rate-limited based on the user's identity.
- Requests exceeding defined limits result in a 429 Too Many Requests response.
- Rate limiting information is stored in-memory but can be easily extended to use Redis as a storage backend for scalability.

## File Structure

- app.py: Main Flask application file.
- config.yaml: Configuration file for rate limits.
- Dockerfile: Dockerfile for building the Flask application.
- docker-compose.yml: Docker Compose configuration for orchestrating the Flask service and Redis.
- start.sh: Script to check Docker and Docker Compose installation and start the service.
- test_service.sh: Script to simulate requests and test rate limiting.
